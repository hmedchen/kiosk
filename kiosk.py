#!/usr/bin/env python3

#--- Imports
import RPi.GPIO as GPIO
import signal
import time
import sys
import os
import board
import neopixel
import pygame
import json
from configparser import ConfigParser
from pygame.locals import *

#--- Konfiguration
SLEEP_TIME = 100 # Millisekunden Schlaf zwischen Sensorabfragen

#--- Pin Definitionen
SENSOR_PIN = 16
LEDS_PIN = board.D12   # NeoPixels must be connected to D10, D12, D18 or D21 to work.

# in config file now
#LED_COLOR=(0,255,50)
#NUM_LEDS = 9
#--- Bilddateien
#BG_IMG="/home/pi/kiosk/img/bg.jpg"
#POPUP_IMG="/home/pi/kiosk/img/popup.gif"

LED_OFF=(0,0,0)

#--- init globals
strip = None
popup = True
window = None
pop_img = None
background = None
config = None


a,b=pygame.init()
print(a,b)
#pygame.display.init()
#--- Grafik-initialisierung
window = pygame.display.set_mode((0,0),pygame.FULLSCREEN)
#window = pygame.display.set_mode((0,0),flags=pygame.FULLSCREEN)

#--- Funktionsdefinitionen
def read_config():
    config={}
    filepath = "."
    config_path = os.path.join(filepath, "kiosk.cfg")
    configfile = ConfigParser(allow_no_value=True)

    try:
        configfile.read_file(open(config_path))
    except:
        print("Error: Cannot open config file.")
        exit(1)
    for section in configfile.sections():

        config.update({section: {} })
        for item in configfile.items(section):
            config[section].update({item[0]: item[1]})
    return (config)

def init():
    global strip
    global window
    global background
    global pop_img
    global config

    print("init..")

    file_path="."

    # initialize signal
    signal.signal(signal.SIGTERM, catch_signal)
    signal.signal(signal.SIGINT, catch_signal)
    signal.signal(signal.SIGABRT, catch_signal)

    config=read_config()
    print("Config:",type(config),config)

    # init neopixels
    strip = neopixel.NeoPixel(LEDS_PIN, int(config["leds"]["num_leds"]))

    #--- Pin Modus setzen
    GPIO.setmode(GPIO.BCM)

    #--- Input/Output Modus fuer belegte Pins setzen
    GPIO.setup(SENSOR_PIN, GPIO.IN)

    # Hintergrundbild laden
    background = pygame.image.load(
                 os.path.join(file_path,"img",config["images"]["image_background"]))

    # Hintergrundbild auf Bildschirmgroesse strecken
    background = pygame.transform.scale(background,window.get_size())

    # Popup-bild laden
    pop_img = pygame.image.load(
              os.path.join(file_path,"img",config["images"]["image_popup"]))

    # Hintergrundbild darstellen
    window.blit(background, (0, 0)) #Replace (0, 0) with desired coordinates
    pygame.display.flip()



def toggle_popup():
    ''' popup-bild darstellen '''
    global popup
    global window
    global pop_img

    print("plop")
    if popup:
        window.blit(pop_img, ((window.get_width() - pop_img.get_width())//2,
                                 (window.get_height() - pop_img.get_width())//2))
        pygame.display.flip()
        popup=False
    else:
        window.blit(background, (0, 0)) #Replace (0, 0) with desired coordinates
        pygame.display.flip()
        popup=True

def catch_signal(a,b):
    ''' Cleanup nach Beendigungs-Signal '''
    print ("Beende...")
    strip.fill(LED_OFF)
    strip.show()
    GPIO.cleanup()
    exit(0)

def strip_cycle():
    num_leds=int(config["leds"]["num_leds"])
    col=pygame.Color(config["leds"]["led_color"])

    for i in range(num_leds):
        strip[i] = (col.r,col.g,col.b)
        strip.show()
        pygame.time.wait(int(config["timings"]["cycle_on_time"]))

    time.sleep(int(config["timings"]["popup_time"]))

    for i in range(num_leds):
        strip[i] = (LED_OFF)
        strip.show()
        pygame.time.wait(int(config["timings"]["cycle_off_time"]))

def bewegung_erkannt(channel):
    ''' wird aufgerufen sobald der Sensor eine Bewegung erkennt '''
    # Hier kann alternativ eine Anwendung/Befehl etc. gestartet werden.
    print('Es gab eine Bewegung!')
    toggle_popup()
    strip_cycle()
    toggle_popup()
    time.sleep(int(config["timings"]["off_time"]))

def main():
    print("main")

    GPIO.add_event_detect(SENSOR_PIN , GPIO.RISING, callback=bewegung_erkannt)
    while True:
        for e in pygame.event.get():
            if e.type == pygame.QUIT or e.type == pygame.MOUSEBUTTONDOWN:
                pygame.quit()
                exit(0)
            elif e.type == pygame.KEYDOWN:
                if e.key == pygame.K_SPACE:
                    toggle_popup()

        time.sleep(0.1)

#----- MAIN ------
#----- fuehre main() funktion aus
if __name__ == "__main__":
    init()
    main()
