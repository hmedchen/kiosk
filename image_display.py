import pygame
import sys
from pygame.locals import *
pygame.init()

BG_IMG="img/bg.jpg"
POPUP_IMG="img/popup.gif"
window = pygame.display.set_mode(flags=pygame.FULLSCREEN)

popup = True

pop_img = pygame.image.load(POPUP_IMG)
# Hintergrundbild laden
background = pygame.image.load(BG_IMG)
# Hintergrundbild auf Bildschirmgroesse strecken
background = pygame.transform.scale(background,window.get_size())

def toggle_popup():
    global popup
    print("plop")
    if popup:
        window.blit(pop_img, ((window.get_width() - pop_img.get_width())//2,
                                 (window.get_height() - pop_img.get_width())//2))
        pygame.display.flip()
        popup=False
    else:
        window.blit(background, (0, 0)) #Replace (0, 0) with desired coordinates
        pygame.display.flip()
        popup=True


# Initial display of background
window.blit(background, (0, 0)) #Replace (0, 0) with desired coordinates
pygame.display.flip()

# loop
while True:
    for e in pygame.event.get():
        if e.type == pygame.QUIT or e.type == pygame.MOUSEBUTTONDOWN:
            pygame.quit()
            exit(0)
        elif e.type == pygame.KEYDOWN:
            if e.key == pygame.K_SPACE:
                toggle_popup()
